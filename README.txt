Overview:

Drupal's core Field API defines a hook for field access, hook_field_access,
that is restrictive as all module developers are aware. In other words, given
field F, if a single module implementing hook_field_access returns a Boolean
false, thus denying access to the field, Drupal will deny access to the field
even if there is another module that absolutely requires access to the field.

Normally, this is acceptable but sometimes a module might want to override the
grants for a field denied access by another module. Or, a module might contain
logic to grant access to a field but another completely independent module has
denied access. Because of the restrictive nature of the field_access function,
which internally invokes hook_field_access, access will be denied.

There are numerous scenarios that are similar in nature to
hook_file_download_access and hook_file_download_access_alter. For a possible
scenario, read on.

How it works:

This module works with a play on of the Adapter Pattern, procedural style. It
defines 2 hooks, hook_field_access_alter_field_access and
hook_field_access_alter_field_access_alter.

Internally, the module implements hook_field_access. This implementation acts
as the adapter. The implementation then simply invokes
hook_field_access_alter_field_access. That hook is simply a replacement for
core's hook_field_access. Third party modules that want other modules to alter
their grants should simply implement that new hook instead of core's.

After invoking hook_field_access_alter_field_access, the implementation saves
the grant for each implementing module and then invokes
hook_field_access_alter_field_access_alter, passing the complete collection of
grants.

Finally, the implementation checks if there is a single module that denied
access. If there is, it also denies access. Otherwise, access is granted. So
it maintains core's restrictive nature.
