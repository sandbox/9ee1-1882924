<?php

/**
 * @file
 * Field Access Alter module hook definition file.
 */

/**
 * Field Access Alter Module Field Access Hook.
 *
 * @param string   $operation
 *   An access operation.
 * @param array    $field
 *   A collection, adhering to Drupal's field API.
 * @param string   $entity_type
 *   The entity type the field is referenced by.
 * @param stdClass $entity
 *   The entity the field is referenced by.
 * @param stdClass $user
 *   The user accessing the field.
 *
 * @return bool
 *   A boolean true if the field access is granted. A boolean false otherwise.
 */
function hook_field_access_alter_field_access($operation, $field, $entity_type, $entity, $user) {
  // ... Implementation code.
}

/**
 * Field Access Alter Module Field Access Alter Hook.
 *
 * @param array    $grants
 *   A key/value collection of grants.
 * @param string   $operation
 *   An access operation.
 * @param array    $field
 *   A collection, adhering to Drupal's field API.
 * @param string   $entity_type
 *   The entity type the field is referenced by.
 * @param stdClass $entity
 *   The entity the field is referenced by.
 * @param stdClass $user
 *   The user accessing the field.
 *
 * @return bool
 *   A boolean true if the field access is granted. A boolean false otherwise.
 */
function hook_field_access_alter_field_access_alter(&$grants, $operation, $field, $entity_type, $entity, $user) {
  // ... Implementation code.
}
